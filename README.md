# **Minimierung der Auswirkung von Windlasten auf Turmkräne mithilfe von IoT**
## **Design Driven Project - final documentation**
### Noureldeen Nagm, Dominik Leitner 	
<br>

![00_2021_WiSe_DDP_introduction_Nagm_Leitner.png](./media/00_2021_WiSe_DDP_introduction_Nagm_Leitner.png)
<br>

**problem statment**  
 With crane operation the wind conditions can present a potential danger that shouldn’t be underestimated.
<br>


**vision**  
 A doable protocol that relies on the most advanced IOT technologies which aims to not only minimize the risk of crane failures but also to decrease crane operation stoppages due to wind conditions. This approach will focus on the most significant yet neglected factor in wind speed assessment process which is the surface area of the load.  

**final presentation:**  
to find an overview of the project as well as the final presentation slides (.pptx):   
[click here](https://gitlab.com/donnoxin/minimierung-der-auswirkung-von-windlasten-auf-turmkraene-mithilfe-von-iot) 
<br>
<br> 
___

## **building blocks**  
<br>

In order to make the **MVP** work we have to adapt it to the context of a **small-scale environment/prototype**. Some of the **input parameters**/assumptions have to be **idealized**/set aside.

**main components**  
wind direction vane  
real time object detection repository  
servo motor  
RaspberryPi (+monitor, mouse and keyboard)  

**goal**  
- get wind direction from wind direction vane  
- get object orientation from real time object orientation repository  
- with a servo motor rotate object into the wind with the smaller side  
&rarr; crane can be operated during higher winds for a longer time
<br>
<br>  
___

## **wind direction vane - tinkerforge weather station WS-6147** 
<br>

[buy here](https://www.tinkerforge.com/en/shop/outdoor-weather-station-ws-6147.html)  
[datasheet](https://www.argentdata.com/files/80422_datasheet.pdf)

sensors important for prototyping:  
Anemometer  
Wind direction sensor  
<br> 
**output data:**  

wind direction in 16 discrete steps:  
N, NNE, NE, ENE, E, ESE, SE, SSE, S, SSW, SW, WSW, W, WNW, NW, NNW  

wind speed:  
m/s with 0.1m/s resolution 

gust speed:  
m/s with 0.1m/s resolution  
<br>
### **hardware setup**  
The weather station is intended to be used together with the [TF Outdoor Weather Bricklet](https://www.tinkerforge.com/en/shop/outdoor-weather-bricklet.html)  
and the [TF Master Brick](https://www.tinkerforge.com/de/shop/bricks/master-brick.html).  

**TF Outdoor Weather Bricklet**   
The Outdoor Weather Bricklet is equipped with a 433MHz receiver capable of receiving data from outdoor weather stations.  

**TF Master Brick**  
Bricks are the base modules of each Tinkerforge system, their main purpose is to attach Bricklets to the system.  

connect the **TF Outdoor Weather Bricklet** to the **TF Master Brick**  
&rarr; then connect the **TF Master Brick** to the **RaspberryPi** via a **USB mini-B cable**  
<br>
### **software setup**  
the software setup steps can be also found here: [instruction video](https://www.youtube.com/watch?v=6qQJpjRHYHU&list=WL&index=43&ab_channel=DIYDeutschland) - DIY Deutschland  

All the software installation steps are done on the RaspberryPi  
<br>
1.) install the package manager using the command terminal:  
```
sudo apt-get install gdebi
```  
2.) use the package manager to install the **brick daemon**, **brick viewer** and **Tisch-Wetterstation** into the following location: 
```
/home/pi
```  
![01_home_pi_folder.png](./media/tinkerforge_weather_station/01_home_pi_folder.png)  

**brick daemon (brickd)** - program running in the back:  
[software download link](https://www.tinkerforge.com/de/doc/Software/Brickd.html) - AMD64 - Linux  
bridge between brick/bricklets and API bindings
transfers data between USB connection and TCP/IP socket (possibility to program with different languages)  

**brick viewer (brickv)** – updates+UID:  
[software download link](https://www.tinkerforge.com/de/doc/Software/Brickv.html) - AMD64 - Linux    
graphical interface for testing bricks/bricklets (main features + control interface)  
additionally: brickv can be used to calibrate the analog-to-digital converter  
&rarr; improve measurement quality

&rarr; open the brick viewer and connect to the Master Brick  
![11_brick_viewer_connect.png](./media/tinkerforge_weather_station/11_brick_viewer_connect.png)  
<br>
**Tisch-Wetterstation**  
UI to display data output  
[software download link](https://www.tinkerforge.com/de/doc/Downloads.html)  
to autostart the UI move the file into the following location:  
```
/home/pi/.config/autostart
```  
![02_weather_station_autostart.png](./media/tinkerforge_weather_station/02_weather_station_autostart.png)  
&rarr; if done correctly the Tabletop Weather Station Demo should be initialized:  

![03_tabletop_weather_station.png](./media/tinkerforge_weather_station/03_tabletop_weather_station.png)  
<br>
**Message Queuing Telemetry Transport (MQTT) - API bindings**  
[software download link](https://www.tinkerforge.com/en/doc/Downloads.html#downloads-bindings-examples)  
MQTT bindings allow you to control Bricks and Bricklets using the MQTT protocol.  
messaging protocol for constrained devices with low bandwidth 

**requirements:**  
Python 3.4 or newer  
Paho MQTT 1.3.1 or newer
```
sudo apt install python
```
```
sudo apt install paho-mqtt
```

1.) open .zip and extract **tinkerforge_mqtt.py** into the following location:  
```
/usr/local/bin/
```  
![04_usr_local_bin.png](./media/tinkerforge_weather_station/04_usr_local_bin.png)  

if help commands needed use the command terminal:  
```  
python /usr/local/bin/tinkerforge_mqtt --h
``` 
2.) create a .txt file with the following name:
```
outdoor_weather_config.txt
```
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.1) content of the file:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;initial parameters to read MQTT correctly  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;replace **XYZ** by the **UID** of the Weather Bricklet  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&rarr; get UID from brick viewer 
```
{
        "tinkerforge/request/outdoor_weather_bricklet/XYZ/set_station_callback_configuration": {"enable_callback": true},
        "tinkerforge/request/outdoor_weather_bricklet/XYZ/set_sensor_callback_configuration": {"enable_callback": true},
        "tinkerforge/register/outdoor_weather_bricklet/XYZ/station_data": {"register": true},
        "tinkerforge/register/outdoor_weather_bricklet/XYZ/sensor_data": {"register": true}
}
```

![05_brick_viewer.png](./media/tinkerforge_weather_station/05_brick_viewer.png)  

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.2) locate outdoor_weather_config.txt in:
```
/usr/local/bin/
```

![09_usr_local_bin_2.png](./media/tinkerforge_weather_station/09_usr_local_bin_2.png)  

3.) autostart of tinkerforge_MQTT - type in command terminal:
```
crontab -e
```  
![06_terminal_crontab.png](./media/tinkerforge_weather_station/06_terminal_crontab.png)  

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3.1) change file content to:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;replace XYZ with correct parameters
```
***** /usr/bin/python3 /usr/local/bin/tinkerforge_mqtt --debug  
--broker-host XYZ --broker-username XYZ --broker-password XYZ
--init-file /usr/local/bin/outdoor_weather_config.txt >> ~/cron.log 2<&1
```  
![07_terminal_crontab_2.png](./media/tinkerforge_weather_station/07_terminal_crontab_2.png)  

4.) run the **tinkerforge_mqtt** script, configured to connect to your Brick Daemon and MQTT broker, using the command line switches  
[MQTT - outdoor weather bricklet](https://www.tinkerforge.com/en/doc/Software/Bricklets/OutdoorWeather_Bricklet_MQTT.html)  
[testing an example: MQTT - API bindings](https://www.tinkerforge.com/en/doc/Software/API_Bindings_MQTT.html)
```
tinkerforge_mqtt --debug --broker-host <broker address>
```
a broker address can be generated here: [HiveMQ](http://www.hivemq.com/demos/websocket-client/) or [MQTT.fx](https://mqttfx.jensd.de/)  

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &rarr; in addition to the software setup, the weather station has to be triggered  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;manually in node-red to start transmitting data as well:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &rarr; see later chapter: **data flow I - node-red**  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*2.) trigger the tinkerforge weather station into sending live weather data*  

![10_weather_station_start_transmitting.png](./media/tinkerforge_weather_station/10_weather_station_start_transmitting.png)

5.) reboot the RaspberryPi 
```
sudo reboot
```
5.) check if installation steps were successful by subscribing to a viewer e.g: [HiveMQ](http://www.hivemq.com/demos/websocket-client/) or [MQTT.fx](https://mqttfx.jensd.de/)  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &rarr;  **.json** with output data from tinkerforge weather station  

**! step only executable if additional steps were done as well !**  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &rarr; in addition to the software setup the weather station has to be triggered in node-red to start transmitting data as well:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &rarr; see later chapter: **data flow I - node-red**  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*2.) trigger the tinkerforge weather station into sending live weather data)* 

![08_mqqtt_fx_output.png](./media/tinkerforge_weather_station/08_mqqtt_fx_output.png)
<br>
<br>
___
## **real time object detection repository - rtom**
<br> 

the software setup steps can be also found here: [instruction video](https://www.youtube.com/watch?v=lbgl2u6KrDU&list=WL&index=35&ab_channel=Pysource) - Pysource  
the code is modified to fit the needs of our prototyping goals  

All the software installation steps are done on a **second computer**  
&rarr; compute intensive repository (compute power of RaspberryPi not be enough)  

**output data:**  
length  
width   
angle  

**requirements:**   
anaconda virtual environment  
repository   
external webcam  
<br>
### **software setup** 

**anaconda virtual environment**  
1.) create a new anaconda virtual environment called:
```
rtom
```  
![01_anaconda_environment.png](./media/rtom_repository/01_anaconda_environment.png)  

2.) install the opencv library using the windows command terminal:  
```
opencv-contrib-python
```

3.) install necessary libraries/dependencies 
```
numpy
opencv
py-opencv
```
<br> 

**real time object detection repository**  
1.) create a new folder in your **repos** directory called:
```
measure_object_size
```
the folder will contain two .py files which are needed to run the repository and create an output:
``` 
output_camera_pixels.py    - creates output data
object_detector.py         - object detection algorithm
```
2.) create a new file within the **measure_object_size** folder called:
```
output_camera_pixels.py
```
3.) copy the following code into the created **output_camera_pixels.py** file:  
[output_camera_pixels.py - Gitlab](./core/output_camera_pixels.py) 
```python
import cv2
from object_detector import *
import numpy as np

# Load Cap
cap = cv2.VideoCapture(0)
cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)

while True: 
    _, img = cap.read()

    # Load Object Detector
    detector = HomogeneousBgDetector()

    contours = detector.detect_objects(img) 

    # Draw objects boundaries
    for cnt in contours:
        # Get rect
        rect = cv2.minAreaRect(cnt)
        (x,y), (w, h), angle = rect

        # Display rectangle
        box = cv2.boxPoints(rect)
        box = np.int0(box)

        cv2.circle(img, (int(x), int(y)), 5, (0, 0, 255), -1)
        cv2.polylines(img, [box], True, (255, 0, 0), 2)
        cv2.putText(img, "Width {} pxl".format(round(w)), (int(x - 100), int(y - 10)), cv2.FONT_HERSHEY_PLAIN, 2, (100, 200, 0), 2)
        cv2.putText(img, "Height {} pxl".format(round(h)), (int(x - 100), int(y + 30)), cv2.FONT_HERSHEY_PLAIN, 2, (100, 200, 0), 2)
        cv2.putText(img, "Angle {} deg".format(round(angle, 1)), (int(x - 100), int(y - 35)), cv2.FONT_HERSHEY_PLAIN, 2, (100, 200, 0), 2)

        print(box)
        print(angle)


    cv2.imshow("Image", img)
    key = cv2.waitKey(1) 
    if key == 27:
        break

cap.release()
cv2.destroyAllWindows()
```
4.) create a new file within the **measure_object_size** folder called:
```
object_detector.py
```
5.) copy the following code into the created **object_detector.py** file:  
[object_detector.py - Gitlab](./core/object_detector.py)
```python
import cv2


class HomogeneousBgDetector():
    def __init__(self):
        pass

    def detect_objects(self, frame):
        # Convert Image to grayscale
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        # Create a Mask with adaptive threshold
        mask = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY_INV, 19, 5)

        # Find contours
        contours, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        #cv2.imshow("mask", mask)
        objects_contours = []

        for cnt in contours:
            area = cv2.contourArea(cnt)
            if area > 2000:
                #cnt = cv2.approxPolyDP(cnt, 0.03*cv2.arcLength(cnt, True), True)
                objects_contours.append(cnt)

        return objects_contours

    # def get_objects_rect(self):
    #     box = cv2.boxPoints(rect)  # cv2.boxPoints(rect) for OpenCV 3.x
    #     box = np.int0(box)
```
6.) addressing the external webcam as input device via device manager:
```
 in: windows/device manager/cameras 
 select your external webcam (deselect your internal webcam)
```  
![02_external_webcam.png](./media/rtom_repository/02_external_webcam.png)  

**running the repository via anaconda terminal**  
1.) open a terminal in the rtom environment in anaconda  
2.) go to the directory where the **output_camera_pixels.py** is located  
```
C:\Users\XY\repos\measure_object_size
```
3.) run the repository with command terminal:
```
python output_camera_pixels.py
```
![03_repository_directory.png](./media/rtom_repository/03_repository_directory.png)
**&rarr; if done correctly**  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.) tab with live video feed opens  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.) command terminal with output data of detected object opens  
![04_output_data.png](./media/rtom_repository/04_output_data.png)
<br>
<br>
___

## **Pose estimation - Aruco marker**  
<br>
Using the previous method we were able to get the angle of orientation for all the objects in the camera view. Here we will get the orientation of only one specific object that resembles our rigged load using Aruco markers.

[tutorial page](https://automaticaddison.com/how-to-perform-pose-estimation-using-an-aruco-marker/?fbclid=IwAR0VIGb5652RN-BfWC9RhPV-FMH7rg6RUuLkfYpY1_0VgGy-CVANSgJtFmo)  

![01_Aruco-pose-detection.JPG](./media/pose_estimation/01_Aruco-pose-detection.JPG) 

**Prerequisites**  
- Aruco marker.
- Open CV (python).
- Scipy.
- Built in or external webcam.

**Creating an Aruco marker** 
[Aruco marker generator Link](https://chev.me/arucogen/)  
- Dictionary: Original ArUco.
- Marker ID: 1.
- Marker Size, mm: 100.

![02_Ar_generator.JPG](./media/pose_estimation/02_Ar_generator.JPG) 

**Setting up anaconda environment** 

1.) Create a python 3.8 anaconda invironment an name it "Aruco".

![03_Conda.JPG](./media/pose_estimation/03_Conda.JPG) 

2.) Install openCV by Opening the Terminal and typing the command:
```
pip install opencv-contrib-python
```
3.) Install scipy by Opening the Terminal and typing the command:
```
pip install pip install scipy
``` 

**camera callibration**   

1.) Print a chess board on A4 paper.
[Chessboard image Link](https://github.com/opencv/opencv/blob/master/doc/pattern.png)  
2.) Take as many photos of the printed chessboard from different angles and distances.
Here is an example of a picture I took from my built in camera.

![04_Chess.jpg](./media/pose_estimation/04_Chess.jpg) 

3.) Save the photos in the same folder directory you will save the code in.

4.) Copy the bellow code and paste it in a python file named "camera_calibration.py" and save it in the same directory with the photos of the chessboard.

[camera_calibration.py](./core/camera_calibration.py)  
```python
#!/usr/bin/env python
  
'''
Welcome to the Camera Calibration Program!
  
This program:
  - Performs camera calibration using a chessboard.
'''
 
from __future__ import print_function # Python 2/3 compatibility 
import cv2 # Import the OpenCV library to enable computer vision
import numpy as np # Import the NumPy scientific computing library
import glob # Used to get retrieve files that have a specified pattern
 
# Project: Camera Calibration Using Python and OpenCV
# Date created: 12/19/2021
# Python version: 3.8
  
# Chessboard dimensions
number_of_squares_X = 10 # Number of chessboard squares along the x-axis
number_of_squares_Y = 7  # Number of chessboard squares along the y-axis
nX = number_of_squares_X - 1 # Number of interior corners along x-axis
nY = number_of_squares_Y - 1 # Number of interior corners along y-axis
square_size = 0.025 # Size, in meters, of a square side 
  
# Set termination criteria. We stop either when an accuracy is reached or when
# we have finished a certain number of iterations.
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001) 
 
# Define real world coordinates for points in the 3D coordinate frame
# Object points are (0,0,0), (1,0,0), (2,0,0) ...., (5,8,0)
object_points_3D = np.zeros((nX * nY, 3), np.float32)  
  
# These are the x and y coordinates                                              
object_points_3D[:,:2] = np.mgrid[0:nY, 0:nX].T.reshape(-1, 2) 
 
object_points_3D = object_points_3D * square_size
 
# Store vectors of 3D points for all chessboard images (world coordinate frame)
object_points = []
  
# Store vectors of 2D points for all chessboard images (camera coordinate frame)
image_points = []
  
def main():
      
  # Get the file path for images in the current directory
  images = glob.glob('*.jpg')
      
  # Go through each chessboard image, one by one
  for image_file in images:
   
    # Load the image
    image = cv2.imread(image_file)  
  
    # Convert the image to grayscale
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)  
  
    # Find the corners on the chessboard
    success, corners = cv2.findChessboardCorners(gray, (nY, nX), None)
      
    # If the corners are found by the algorithm, draw them
    if success == True:
  
      # Append object points
      object_points.append(object_points_3D)
  
      # Find more exact corner pixels       
      corners_2 = cv2.cornerSubPix(gray, corners, (11,11), (-1,-1), criteria)       
        
      # Append image points
      image_points.append(corners_2)
  
      # Draw the corners
      cv2.drawChessboardCorners(image, (nY, nX), corners_2, success)
  
      # Display the image. Used for testing.
      cv2.imshow("Image", image) 
      
      # Display the window for a short period. Used for testing.
      cv2.waitKey(1000) 
                                                                                                                      
  # Perform camera calibration to return the camera matrix, distortion coefficients, rotation and translation vectors etc 
  ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(object_points, 
                                                    image_points, 
                                                    gray.shape[::-1], 
                                                    None, 
                                                    None)
 
  # Save parameters to a file
  cv_file = cv2.FileStorage('calibration_chessboard.yaml', cv2.FILE_STORAGE_WRITE)
  cv_file.write('K', mtx)
  cv_file.write('D', dist)
  cv_file.release()
  
  # Load the parameters from the saved file
  cv_file = cv2.FileStorage('calibration_chessboard.yaml', cv2.FILE_STORAGE_READ) 
  mtx = cv_file.getNode('K').mat()
  dst = cv_file.getNode('D').mat()
  cv_file.release()
   
  # Display key parameter outputs of the camera calibration process
  print("Camera matrix:") 
  print(mtx) 
  
  print("\n Distortion coefficient:") 
  print(dist) 
    
  # Close all windows
  cv2.destroyAllWindows() 
      
if __name__ == '__main__':
  print(__doc__)
  main()
  ```
5.) Move to the directory through the terminal and run the code by typing the bellow command:
```
Python camera_calibration.py
```
This should open a window that looks like this:

![05_callibration.JPG](./media/pose_estimation/05_callibration.JPG) 

and will create a yaml file in the folder directory that looks like this:

![06_yaml.JPG](./media/pose_estimation/06_yaml.JPG) 

**Running the code** 

1.) Copy the bellow code and paste it in a python file named "aruco_marker_pose_estimator.py" and save it in the same directory.

[aruco_marker_pose_estimator.py](./core/aruco_marker_pose_estimator.py)  
```python
#!/usr/bin/env python
  
'''
Welcome to the ArUco Marker Pose Estimator!
  
This program:
  - Estimates the pose of an ArUco Marker
'''
  
from __future__ import print_function # Python 2/3 compatibility
import cv2 # Import the OpenCV library
import numpy as np # Import Numpy library
from scipy.spatial.transform import Rotation as R
import math # Math library
 
# Project: ArUco Marker Pose Estimator
# Date created: 12/21/2021
# Python version: 3.8
 
# Dictionary that was used to generate the ArUco marker
aruco_dictionary_name = "DICT_ARUCO_ORIGINAL"
 
# The different ArUco dictionaries built into the OpenCV library. 
ARUCO_DICT = {
  "DICT_4X4_50": cv2.aruco.DICT_4X4_50,
  "DICT_4X4_100": cv2.aruco.DICT_4X4_100,
  "DICT_4X4_250": cv2.aruco.DICT_4X4_250,
  "DICT_4X4_1000": cv2.aruco.DICT_4X4_1000,
  "DICT_5X5_50": cv2.aruco.DICT_5X5_50,
  "DICT_5X5_100": cv2.aruco.DICT_5X5_100,
  "DICT_5X5_250": cv2.aruco.DICT_5X5_250,
  "DICT_5X5_1000": cv2.aruco.DICT_5X5_1000,
  "DICT_6X6_50": cv2.aruco.DICT_6X6_50,
  "DICT_6X6_100": cv2.aruco.DICT_6X6_100,
  "DICT_6X6_250": cv2.aruco.DICT_6X6_250,
  "DICT_6X6_1000": cv2.aruco.DICT_6X6_1000,
  "DICT_7X7_50": cv2.aruco.DICT_7X7_50,
  "DICT_7X7_100": cv2.aruco.DICT_7X7_100,
  "DICT_7X7_250": cv2.aruco.DICT_7X7_250,
  "DICT_7X7_1000": cv2.aruco.DICT_7X7_1000,
  "DICT_ARUCO_ORIGINAL": cv2.aruco.DICT_ARUCO_ORIGINAL
}
 
# Side length of the ArUco marker in meters 
aruco_marker_side_length = 0.0785
 
# Calibration parameters yaml file
camera_calibration_parameters_filename = 'calibration_chessboard.yaml'
 
def euler_from_quaternion(x, y, z, w):
  """
  Convert a quaternion into euler angles (roll, pitch, yaw)
  roll is rotation around x in radians (counterclockwise)
  pitch is rotation around y in radians (counterclockwise)
  yaw is rotation around z in radians (counterclockwise)
  """
  t0 = +2.0 * (w * x + y * z)
  t1 = +1.0 - 2.0 * (x * x + y * y)
  roll_x = math.atan2(t0, t1)
      
  t2 = +2.0 * (w * y - z * x)
  t2 = +1.0 if t2 > +1.0 else t2
  t2 = -1.0 if t2 < -1.0 else t2
  pitch_y = math.asin(t2)
      
  t3 = +2.0 * (w * z + x * y)
  t4 = +1.0 - 2.0 * (y * y + z * z)
  yaw_z = math.atan2(t3, t4)
      
  return roll_x, pitch_y, yaw_z # in radians
 
def main():
  """
  Main method of the program.
  """
  # Check that we have a valid ArUco marker
  if ARUCO_DICT.get(aruco_dictionary_name, None) is None:
    print("[INFO] ArUCo tag of '{}' is not supported".format(
      args["type"]))
    sys.exit(0)
 
  # Load the camera parameters from the saved file
  cv_file = cv2.FileStorage(
    camera_calibration_parameters_filename, cv2.FILE_STORAGE_READ) 
  mtx = cv_file.getNode('K').mat()
  dst = cv_file.getNode('D').mat()
  cv_file.release()
     
  # Load the ArUco dictionary
  print("[INFO] detecting '{}' markers...".format(
    aruco_dictionary_name))
  this_aruco_dictionary = cv2.aruco.Dictionary_get(ARUCO_DICT[aruco_dictionary_name])
  this_aruco_parameters = cv2.aruco.DetectorParameters_create()
   
  # Start the video stream
  cap = cv2.VideoCapture(0)
   
  while(True):
  
    # Capture frame-by-frame
    # This method returns True/False as well
    # as the video frame.
    ret, frame = cap.read()  
     
    # Detect ArUco markers in the video frame
    (corners, marker_ids, rejected) = cv2.aruco.detectMarkers(
      frame, this_aruco_dictionary, parameters=this_aruco_parameters,
      cameraMatrix=mtx, distCoeff=dst)
       
    # Check that at least one ArUco marker was detected
    if marker_ids is not None:
 
      # Draw a square around detected markers in the video frame
      cv2.aruco.drawDetectedMarkers(frame, corners, marker_ids)
       
      # Get the rotation and translation vectors
      rvecs, tvecs, obj_points = cv2.aruco.estimatePoseSingleMarkers(
        corners,
        aruco_marker_side_length,
        mtx,
        dst)
         
      # Print the pose for the ArUco marker
      # The pose of the marker is with respect to the camera lens frame.
      # Imagine you are looking through the camera viewfinder, 
      # the camera lens frame's:
      # x-axis points to the right
      # y-axis points straight down towards your toes
      # z-axis points straight ahead away from your eye, out of the camera
      for i, marker_id in enumerate(marker_ids):
       
        # Store the translation (i.e. position) information
        transform_translation_x = tvecs[i][0][0]
        transform_translation_y = tvecs[i][0][1]
        transform_translation_z = tvecs[i][0][2]
 
        # Store the rotation information
        rotation_matrix = np.eye(4)
        rotation_matrix[0:3, 0:3] = cv2.Rodrigues(np.array(rvecs[i][0]))[0]
        r = R.from_matrix(rotation_matrix[0:3, 0:3])
        quat = r.as_quat()   
         
        # Quaternion format     
        transform_rotation_x = quat[0] 
        transform_rotation_y = quat[1] 
        transform_rotation_z = quat[2] 
        transform_rotation_w = quat[3] 
         
        # Euler angle format in radians
        roll_x, pitch_y, yaw_z = euler_from_quaternion(transform_rotation_x, 
                                                       transform_rotation_y, 
                                                       transform_rotation_z, 
                                                       transform_rotation_w)
         
        roll_x = math.degrees(roll_x)
        pitch_y = math.degrees(pitch_y)
        yaw_z = math.degrees(yaw_z)
        print("transform_translation_x: {}".format(transform_translation_x))
        print("transform_translation_y: {}".format(transform_translation_y))
        print("transform_translation_z: {}".format(transform_translation_z))
        print("roll_x: {}".format(roll_x))
        print("pitch_y: {}".format(pitch_y))
        print("yaw_z: {}".format(yaw_z))
        print()
         
        # Draw the axes on the marker
        cv2.aruco.drawAxis(frame, mtx, dst, rvecs[i], tvecs[i], 0.05)
     
    # Display the resulting frame
    cv2.imshow('frame',frame)
          
    # If "q" is pressed on the keyboard, 
    # exit this loop
    if cv2.waitKey(1) & 0xFF == ord('q'):
      break
  
  # Close down the video stream
  cap.release()
  cv2.destroyAllWindows()
   
if __name__ == '__main__':
  print(__doc__)
  main()
```
2.) Run the code by typing the bellow command in the terminal:
```
Python aruco_marker_pose_estimation.py
```
The output should look like this:

![07_problems.JPG](./media/pose_estimation/07_problems.JPG)  

**Problems with the output** 

1.) As you can see in the above photo the result is an orientation in the x (roll), y (pitch) and z (yaw) and we only need the yaw for 2D orientation.

2.) The yaw has only a range from 0 to 180, so any angle above 180 will have a value of negative. 

3.) The results are printed directly with every movement and we need to slow it down in order to process the results.

**Code modifications** 

1.) In order to print only the yaw z we delete the lines that print the other dimensions and rename yaw z to angle of orientation.
```python
print("roll_x: {}".format(roll_x))
        print("pitch_y: {}".format(pitch_y))
```
2.) In order to mape the negative results to positive we create a simple mathimatical formula that adds 360 to any negative result.
```python
if (yaw_z <0):
          print ("angle of orientation: {}".format(yaw_z+360))
        else:
           print("angle of orientation: {}".format(yaw_z))
```       
3.) In order to create a break of 2 seconds between results we add a python sleep formula by imort time at the start of the code.
```python
import time 
```
and adding this line of code after the print line.
```python
time.sleep(2)
```
Full code after modifications: [pose_estimator_modified.py](./core/pose_estimator_modified.py)  

What you get by running the code should be something like this:

![08_final-result.JPG](./media/pose_estimation/08_final-result.JPG) 
<br>
<br>
____

## **node-red interface**
<br>
In order to bring together the individual data strings as well as to output the collected data in a user interface, the use of node-red is the most suitable approach.  

Depending on your machine and use case there are different options to install node-red:

[software download link](https://nodered.org/docs/getting-started/)  

1.) run node-red with command terminal:
```
node-red
```  
![01_start_node_red.png](./media/node_red/01_start_node_red.png)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &rarr; the command terminal starts node-red and gives an overview under which IP the UI runs  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &rarr; copy and paste the IP and enter it in a web browser of your choice (e.g:Google Chrome)
<br> 
<br> 
____

## **rotatable motor - servo motor SG90**  
<br> 
A servo motor is a simple device that consists of a DC motor, gears and a feedback based position control system. The main advantage of a servo motor is its ability to hold the angular position of its shaft.

[Buy here](https://www.roboter-bausatz.de/p/sg90-9g-micro-servomotor)  
[datasheet](http://www.ee.ic.ac.uk/pcheung/teaching/DE1_EE/stores/sg90_datasheet.pdf)
<br> 

**servo motor specifications**   

- weight: 9g  
- dimension: 22.2 x 11.8 x 31mm approx.  
- stall torque: 1.8 kgf·cm  
- operating speed: 0.1 s/60 degree  
- operating voltage: 4.8 V (~5V)  
- dead band width: 10 µs  
- temperature range: 0 ºC – 55 ºC  

&rarr; servo can rotate approximately 180 degrees, 90 in each direction.

![01_servo_motor.jpg](./media/servo_motor/01_servo_motor.jpg)  


**required components** 
- Tower Pro SG90 Servo Motor
- Raspberry Pi
- connecting wires
- power supply
- input devices: mouse + keyboard  
<br>

**servo motor wire configuration to RaspberryPi**  

The wire configuration steps can be also found here: [instruction link](https://www.electronicshub.org/raspberry-pi-servo-motor-interface-tutorial/) - Electronics Hub  
<br>
The SG90 Servo Motor Consists of three Pins: PWM (Orange or Yellow), VCC (Red) and GND (Brown):

1.) The VCC pin must be connected to +5V.  
2.) GND pin must be connected to GND of the power supply.  
3.) PWM or Signal Pin of the Servo Motor must be connected to the PWM Output of the RaspberryPI

![02_Raspberry-Pi-Servo-Motor-Control-Circuit-Diagram.jpg](./media/servo_motor/02_Raspberry-Pi-Servo-Motor-Control-Circuit-Diagram.jpg) 

![03_Raspberry-Pi-Servo-Motor-Control-Circuit-Diagram_2.jpg](./media/servo_motor/03_Raspberry-Pi-Servo-Motor-Control-Circuit-Diagram_2.jpg) 

**data input**  

The servo motor is controlled using PWM control signals.
In PWM technique, you will be sending a pulse of variable width.
The position of the servo motor’s shaft will be set by the width or length of the Pulse.

The frequency of the PWM signal is a fixed value. In our case, SG90 Servo motors have a PWM frequency of 50Hz.  
&rarr; 50Hz i.e. a period of 20ms, the minimum pulse width is 1ms and the maximum pulse width is 2ms. 

![04_raspberry-Pi-Servo-Motor-PWM-Duty-Cycle-Position.jpg](./media/servo_motor/04_raspberry-Pi-Servo-Motor-PWM-Duty-Cycle-Position.jpg)
<br> 
<br>   
____

## **node-red - servo motor test**  
<br> 

In order to test the servo motor add an inject node that simulates the output of the weather station.

![05_node_inject.png](./media/servo_motor/05_node_inject.png)  
&rarr; set type to string  
<br>
**Node-RED-contrib-compass 1.0.1**  
[node description](https://flows.nodered.org/node/node-red-contrib-compass) 

A Node-RED node to convert an angle to a compass point, or the other way around.

Use (node-red-contrib-compass 1.0.1) to convert the cardinal direction data coming from the weather station into a degree angle.

To get the node copy and import the following code:
[04_Node_RED_compass.json](./core/04_Node_RED_compass.json)  
```json 
[{"id":"bec07115.9b64a","type":"compass-point","z":"11289790.c89848","direction":"toCompass","subset":"secondary","language":"en","inputField":"payload","outputField":"payload","name":"","x":620,"y":680,"wires":[["cc38d99.2386a28"]]},{"id":"122e96c9.988c69","type":"inject","z":"11289790.c89848","name":"","topic":"","payload":"270","payloadType":"num","repeat":"","crontab":"","once":false,"onceDelay":0.1,"x":450,"y":680,"wires":[["bec07115.9b64a"]]},{"id":"cc38d99.2386a28","type":"debug","z":"11289790.c89848","name":"Compass point","active":true,"tosidebar":true,"console":false,"tostatus":false,"complete":"payload","targetType":"msg","x":830,"y":680,"wires":[]},{"id":"6e6a70de.f4f7e","type":"compass-point","z":"11289790.c89848","direction":"toDegree","subset":"secondary","language":"en","inputField":"payload","outputField":"payload","name":"","x":620,"y":740,"wires":[["f863e65b.64a378"]]},{"id":"bba77ff8.e726","type":"inject","z":"11289790.c89848","name":"","topic":"","payload":"W","payloadType":"str","repeat":"","crontab":"","once":false,"onceDelay":0.1,"x":450,"y":740,"wires":[["6e6a70de.f4f7e"]]},{"id":"f863e65b.64a378","type":"debug","z":"11289790.c89848","name":"Degree","active":true,"tosidebar":true,"console":false,"tostatus":false,"complete":"payload","targetType":"msg","x":810,"y":740,"wires":[]}]
``` 
![06_node-red-contrib-compass-1.0.1.png](./media/servo_motor/06_node-red-contrib-compass-1.0.1.png)  

&rarr; in the node settings set compass from point to degree.  

<br> 

**360 to 180 degree mapper** 

A custom made Node-RED function node to convert angles that are greater than 180 into their corresponding angles that are within the range of the servo (360-given angle= new angle).

![07_node_function.png](./media/servo_motor/07_node_function.png)   

To get the node create a new function node and copy the following code inside:  
```javascript
if(msg.payload > 180){
    msg.payload = 360 - msg.payload;
}

return msg;
```
<br> 

**angle to frequency mapper** 

A custom made Node-RED function node to map the resulted degree angle into a frequency that acts as a PWM control signal which the servo can process
((angle / 18) +2 = pulse in HZ. 

![08_node_function.png](./media/servo_motor/08_node_function.png)  

To get the node create a new function node and copy the following code inside:  
```javascript
msg.payload = 2 + ( msg.payload / 18 );

return msg;
```
<br> 

**simple compass to display wind direction**  
[node download link](https://discourse.nodered.org/t/simple-compass-to-display-wind-direction/46993) 

A Node-RED node to display a wind direction compass in the dashboard, with standard nodes.

Use the compass node to visualize the degree of rotation and wind direction in both degrees and cardinal directions.

To get the node copy and import the following code:  
[05_simple_compass_UI.json](./core/05_simple_compass_UI.json) 

```json
[{"id":"7954150f.8bb6cc","type":"inject","z":"fd95058a.48e3e8","name":"","props":[{"p":"payload"}],"repeat":"","crontab":"","once":false,"onceDelay":0.1,"topic":"","payload":"242","payloadType":"num","x":170,"y":380,"wires":[["ff02c176.98bbb"]]},{"id":"ff02c176.98bbb","type":"change","z":"fd95058a.48e3e8","name":"Convert Wind Degrees To Text","rules":[{"t":"set","p":"directions","pt":"flow","to":"$exists($flowContext(\"directions\")) ?  $flowContext(\"directions\") : [\"N\", \"NNE\", \"NE\", \"ENE\", \"E\", \"ESE\", \"SE\", \"SSE\", \"S\", \"SSW\", \"SW\", \"WSW\", \"W\", \"WNW\", \"NW\", \"NNW\"] ","tot":"jsonata"},{"t":"set","p":"index","pt":"msg","to":"$string($floor((payload / 22.5) + 0.5) % 16)","tot":"jsonata"},{"t":"set","p":"topic","pt":"msg","to":"$flowContext(\"directions[\"&index&\"]\")","tot":"jsonata"}],"action":"","property":"","from":"","to":"","reg":false,"x":370,"y":420,"wires":[["37a9c647.e58c8a"]]},{"id":"37a9c647.e58c8a","type":"ui_gauge","z":"fd95058a.48e3e8","name":"Wind direction","group":"d538f03c.af247","order":0,"width":"3","height":"3","gtype":"compass","title":"{{msg.topic}}","label":"degrees","format":"{{value}}","min":0,"max":"359","colors":["#00b500","#e6e600","#ca3838"],"seg1":"","seg2":"","x":600,"y":380,"wires":[]},{"id":"d538f03c.af247","type":"ui_group","name":"Wind","tab":"5b337e3b.68826","order":3,"disp":true,"width":"6","collapse":false},{"id":"5b337e3b.68826","type":"ui_tab","name":"Test","icon":"home","order":6,"disabled":false,"hidden":false}]
```  
To see the user interface open the compass via the dashboard

![09_dashboard-3.JPG](./media/servo_motor/09_dashboard-3.JPG) 

<br> 

**node-red-node-pi-gpio**  
[node download link](https://nodered.org/docs/faq/interacting-with-pi-gpio) 

This module is preinstalled with Node-RED when using our install script. It provides a simple way to monitor and control the GPIO pins.

RaspberryPi OS comes preconfigured for this node to work. If you are running a different distribution, such as Ubuntu, some additional install steps may be needed. 

Either use the Node-RED Menu - Manage Palette option to install by copying the name in the search bar and then choose install:
```
node-red-node-pi-gpio
``` 
![10_search-pallete.JPG](./media/servo_motor/10_search-pallete.JPG) 

or run the following command in your Node-RED user directory - typically ~/.node-red  

```
npm i node-red-node-pi-gpio
```  

![11_command-prompt.JPG](./media/servo_motor/11_command-prompt.JPG) 

Use (node-red-node-pi-gpio) rpi gpio out to send the PWM signal to the raspberrypi pin out connected to the servo.

Choose the intended gpio pin out, set the type to PWM output and the frequency to 50 HZ.

![12_gpio-pin.jpg](./media/servo_motor/12_gpio-pin.jpg)

<br>

**The complete Node-RED flow for servo motor - for testing purposes**  

To get the complete node-red flow for the servo motor in order to test your setup copy the bellow code and import it in node-red:
[06_node_red_servo_motor_testing.json](./core/06_node_red_servo_motor_testing.json)

The imported flow should look like this:

![13_node-red.JPG](./media/servo_motor/13_node-red.JPG)

**controlling the servo motor using the dashboard**  

To test the node-red with the servo use the dashboard to enter cardinal directions and watch the servo rotate to the resulted degrees of rotation.
The direction should be one of these:

The main compass quadrant N, E, S, W.

Extended 'Cardinal' options include NE, SE, SW, NW.

Principal + Secondary: NNE, ENE, ESE, SSE, SSW, WSW, WNW, NNW.

The output should look like this:

![14_servo-output.mp4](./media/servo_motor/14_servo-output.mp4)
<br> 
<br> 
____

## **data flow I - node-red**  
<br> 

In the following the set up of the **data flow I** is explained, follow along or download the **full flow**.  
This chapter will set up the following data flows:
- MQTT  
-  tinkerforge weather station: enable callback and register events
- python execution node  
<br>
**full flow** - download the follwing .json from gitlab and import it into the node-red project:  
[node-red full flow download link](./core/01_full_flow_wind_direction_output.json)

![02_node_red_flow_overview.png](./media/node_red/02_node_red_flow_overview.png)

1.) setup of MQTT broker and messaging protocol:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.1) set up the MQTT broker - type in the RaspberryPi command terminal: 
```
apt-get install mosquitto
```
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;introduction on MQTT broker:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[introduction video](https://www.youtube.com/watch?v=EIxdz-2rhLs&t=181s&ab_channel=RuiSantos)  

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&rarr; additional information in [presentation slides](./2021_WiSe_DDP_final_submission_Nagm_Leitner.pptx) - Message Queuing Telemetry Transport (MQTT)   
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;node-red needs a connection to both:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**tinkerforge weather station** - transmit weather data    
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**second computer** - transmit length, width and/or angle of detected object  
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.2) set up MQTT nodes accordingly:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[instruction video](https://www.youtube.com/watch?v=LXA-Ge3-ewo&ab_channel=LearnRobotics)  

![03_mqtt_server.png](./media/node_red/03_mqtt_server.png)  

2.) trigger the tinkerforge weather station into sending live weather data  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.1) Download the follwing .json from gitlab and import it into the node-red &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;project:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[node-red enable callback and register events station/sensor - Gitlab](./core/02_enable_callback_station_sensor.json)  
```json
[{"id":"eedc5c216e322a83","type":"tab","label":"activate_MQTT_feed","disabled":false,"info":""},{"id":"d071251553c9f469","type":"ui_button","z":"eedc5c216e322a83","name":"","group":"453a6e0968fc78a9","order":0,"width":0,"height":0,"passthru":false,"label":"Enable Callbacks","tooltip":"","color":"","bgcolor":"","className":"","icon":"","payload":"{\"enable_callback\":true}","payloadType":"json","topic":"K4n","topicType":"str","x":230,"y":80,"wires":[["fd70db2b7ef0b35a","77091a705db9b0a8"]]},{"id":"207b6f9708753d2e","type":"mqtt out","z":"eedc5c216e322a83","name":"tinkerforge_station","topic":"","qos":"","retain":"","respTopic":"","contentType":"","userProps":"","correl":"","expiry":"","broker":"33a72466eae0c023","x":870,"y":80,"wires":[]},{"id":"fd70db2b7ef0b35a","type":"template","z":"eedc5c216e322a83","name":"Topic for \"Enable station data callbacks\"","field":"topic","fieldType":"msg","format":"handlebars","syntax":"mustache","template":"tinkerforge/request/outdoor_weather_bricklet/{{topic}}/set_station_callback_configuration","output":"str","x":560,"y":60,"wires":[["207b6f9708753d2e"]]},{"id":"77091a705db9b0a8","type":"template","z":"eedc5c216e322a83","name":"Topic for \"Enable sensor data callbacks\"","field":"topic","fieldType":"msg","format":"handlebars","syntax":"mustache","template":"tinkerforge/request/outdoor_weather_bricklet/{{topic}}/set_sensor_callback_configuration","output":"str","x":560,"y":100,"wires":[["207b6f9708753d2e"]]},{"id":"fd96b2855b4d7c2d","type":"mqtt in","z":"eedc5c216e322a83","name":"","topic":"tinkerforge/callback/outdoor_weather_bricklet/K4n/station_data","qos":"2","datatype":"auto","broker":"33a72466eae0c023","nl":false,"rap":true,"rh":0,"inputs":0,"x":360,"y":240,"wires":[["63d5e75b1f8a5a75"]]},{"id":"745d5b8349fe744a","type":"debug","z":"eedc5c216e322a83","name":"","active":true,"tosidebar":true,"console":false,"tostatus":false,"complete":"payload","targetType":"msg","statusVal":"","statusType":"auto","x":960,"y":260,"wires":[]},{"id":"4f1e9771f57cbf16","type":"ui_button","z":"eedc5c216e322a83","name":"","group":"453a6e0968fc78a9","order":0,"width":0,"height":0,"passthru":false,"label":"Register Events","tooltip":"","color":"","bgcolor":"","className":"","icon":"","payload":"{\"register\":true}","payloadType":"json","topic":"K4n","topicType":"str","x":220,"y":160,"wires":[["fe834c6564b37e86","c872cefe93392954"]]},{"id":"1a74ce9d7babdad5","type":"mqtt out","z":"eedc5c216e322a83","name":"tinkerforge_station","topic":"","qos":"","retain":"","respTopic":"","contentType":"","userProps":"","correl":"","expiry":"","broker":"33a72466eae0c023","x":870,"y":160,"wires":[]},{"id":"fe834c6564b37e86","type":"template","z":"eedc5c216e322a83","name":"Topic for Registering for Station Data","field":"topic","fieldType":"msg","format":"handlebars","syntax":"mustache","template":"tinkerforge/register/outdoor_weather_bricklet/{{topic}}/station_data","output":"str","x":550,"y":140,"wires":[["1a74ce9d7babdad5"]]},{"id":"c872cefe93392954","type":"template","z":"eedc5c216e322a83","name":"Topic for Registering for Sensor Data","field":"topic","fieldType":"msg","format":"handlebars","syntax":"mustache","template":"tinkerforge/register/outdoor_weather_bricklet/{{topic}}/sensor_data","output":"str","x":550,"y":180,"wires":[["1a74ce9d7babdad5"]]},{"id":"2009a1b578f3a6d6","type":"mqtt in","z":"eedc5c216e322a83","name":"","topic":"tinkerforge/callback/outdoor_weather_bricklet/K4n/sensor_data","qos":"2","datatype":"auto","broker":"33a72466eae0c023","nl":false,"rap":true,"rh":0,"inputs":0,"x":360,"y":300,"wires":[["63d5e75b1f8a5a75"]]},{"id":"63d5e75b1f8a5a75","type":"json","z":"eedc5c216e322a83","name":"","property":"payload","action":"","pretty":false,"x":760,"y":260,"wires":[["745d5b8349fe744a"]]},{"id":"453a6e0968fc78a9","type":"ui_group","name":"Weather station","tab":"19e4c87c70e1af23","order":1,"disp":true,"width":"6","collapse":false,"className":""},{"id":"33a72466eae0c023","type":"mqtt-broker","name":"","broker":"broker.hivemq.com","port":"1883","clientid":"","autoConnect":true,"usetls":false,"protocolVersion":"4","keepalive":"60","cleansession":true,"birthTopic":"","birthQos":"0","birthPayload":"","birthMsg":{},"closeTopic":"","closeQos":"0","closePayload":"","closeMsg":{},"willTopic":"","willQos":"0","willPayload":"","willMsg":{},"sessionExpiry":""},{"id":"19e4c87c70e1af23","type":"ui_tab","name":"Home","icon":"dashboard","disabled":false,"hidden":false}]
```

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.2) set up button node **enable callback** accordingly:
```
label:      Enable Callbacks
payload:    {"enable_callback":true}    -> JSON
topic:      UID weather station         -> string
```
![06_enable_callbacks.png](./media/node_red/06_enable_callbacks.png)  

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.3) set up button node **register events** accordingly:  
```
label:      Register Events
payload:    {"register":true}           -> JSON
topic:      UID weather station         -> string
```
![07_register_events.png](./media/node_red/07_register_events.png)

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.4) set up **processing function nodes** accordingly:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; **for enable callbacks:**  

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.4.1) Topic for "Enable station data callbacks" 
```
tinkerforge/request/outdoor_weather_bricklet/{{topic}}/set_station_callback_configuration 
```
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.4.2) Topic for "Enable sensor data callbacks"
```
tinkerforge/request/outdoor_weather_bricklet/{{topic}}/set_sensor_callback_configuration
```
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; **for register events:**  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.4.3) Topic for "Registering for Station Data"
```
tinkerforge/register/outdoor_weather_bricklet/{{topic}}/station_data
```
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2.4.4) Topic for "Registering for Sensor Data"
```
tinkerforge/register/outdoor_weather_bricklet/{{topic}}/sensor_data
```
![08_function_node_topics.png](./media/node_red/08_function_node_topics.png)  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.5) set up both MQTT nodes (in/out) accordingly  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&rarr; output message for all:  
- Enable station data callbacks  
- Enable sensor data callbacks  
- Registering for Station Data  
- Registering for Sensor Data
```
"{"enable_callback":true}"
```
![04_enable_callback_sensor_station.png](./media/node_red/04_enable_callback_sensor_station.png)  
<br>

3.) trigger real time object detection repository from **second computer** into sending live object orientation data  

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3.2) set up **python exec node**:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; two repositories to set up the python exec node are available:  
- real time object detection repository  
- Pose estimation - Aruco marker repository  

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3.2.1) python exec node for the  ***real time object detection repository***: 
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[03_python_exec_rtom_repository.json](./core/03_python_exec_rtom_repository.json)
``` 
C:\tools\Anaconda3\Scripts\activate.bat <environment> &&  <python file>
```  
 
replace **environment** with the name of the Anaconda environment:
```
rtom
```  
replace **python file** with the with the full path of the .py file you want to execute, e.g:  
```
python "C:\Users\XYZ\repos\measure_object_size\output_camera_pixels.py"
```
<br>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3.2.2) python exec node for the ***Pose estimation - Aruco marker*** repository:
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[032_python_exec_aruco_repository.json](./core/032_python_exec_aruco_repository.json)  
``` 
C:\tools\Anaconda3\Scripts\activate.bat <environment> && <python file>
```

replace **environment** with the name of the Anaconda environment:
```
aruco
```
replace **python file** with the with the full path of the .py file you want to execute, e.g:  
```
cd "C:\Users\XYZ\repos\aruco_measure_object_size" && python "pose_estimator_modified.py"
```   
![05_python_exec_rtom_repository.png](./media/node_red/05_python_exec_rtom_repository.png)

if the set up steps were successfull and the flow has been deployed, when injected a possible output in the debug tab could look like this:  

![09_python_exec_output.png](./media/node_red/09_python_exec_output.png)
<br>
<br>
____

## **data flow II - node-red**
<br>

In the following the set up of the **data flow II** is explained.
This chapter will set up the internal data flows:
- input nodes  
- processing/function nodes
- output/dashboard nodes

After testing each building block independently and safely importing data from sensor into our node-red flow.  
We need to create a flow that bridges between our input and output nodes, compare this data with each other and successfully influence the mechanical-audio-visual reaction we aim to achieve.
 
![01_missing_bridge.JPG](./media/data_flow&calculations/01_missing_bridge.JPG) 

**controlling the python repo output**   

In order to control the speed of data we are getting from the python code for object orientation we can rely on the time sleep formula we added in the python code or we can add a delay node in node-red and set the pace. For us 1 message per 2 seconds worked fine.

![02_delay.JPG](./media/data_flow&calculations/02_delay.JPG) 

**coverting wind direction data** 

The cardinal direction we are getting from the weather station is in lowcase letters and we need it to be in uppercase in order to be able to convert it to degree. (check the compass point to degree node settings)

In order to do so we use (node-red-contrib-uppercase):
[node link](https://flows.nodered.org/node/node-red-contrib-uppercase)

![03_upper_case.JPG](./media/data_flow&calculations/03_upper_case.JPG) 

**comparing angles data**  

In order to compare between the object orientation and the wind direction we need to follow some simple steps:

1.) Create two function nodes and set the topic as (load of orientation) and the other as (cardinal direction).
To get the nodes copy the bellow codes inside the function nodes:

load of orientation node:
```javascript
msg.payload = msg.payload;
msg.topic = "Load Orientation";
return msg;
```
cardinal direction node:
```javascript
msg.payload = msg.payload;
msg.topic = "Cardinal Direction";
return msg;
```
2.) Install the following pallete (node-red-contrib-combine
):
[node link](https://flows.nodered.org/node/node-red-contrib-combine)

Then use the delta node to subtract the two outputs. In order to do so set topic a to cardinal direction and topic b to load orientation.

![04_delta.JPG](./media/data_flow&calculations/04_delta.JPG)  

3.) In order to avoid getting a negative read we add a function that converts negative values to positive following the same concept we did for the aruco pose estimator by adding 360 to any negative value.

To get the node create a function node and copy the following code inside:
```javascript
if(msg.payload < 0){
    msg.payload = 360 + msg.payload;
}
return msg;
```
4.) You can add a gauge node to view the resulted angle which is the amount of degrees that the servo should rotate. Adjust the gauge range to be between 0-360.

![05_gauge.JPG](./media/data_flow&calculations/05_gauge.JPG) 

<br> 

**visual and audio alert using the dashboard**  

In order to create a visual and audio alert when the load is facing the wind direction with its larger surface area we need to follow the following simple steps:
  
1.) Create a function node that acts like a trigger for the alarm. When the difference between the angles received from the delta node is between 70-110 the alert is activated which is in a form of the sentence (wind is normal to load). 

To get the node create a function node and copy the following code inside:
```javascript
if(msg.payload > 70)
if(msg.payload < 110){
    msg.payload = "WIND NORMAL TO LOAD";
}
return msg;
```
2.) Add text and audio output nodes and connect them to the trigger function node to play the alert.

![07_alert.jpg](./media/data_flow&calculations/07_alert.jpg)  

To view the output assign the following nodes to the same group:
- run code button node
- load orientation compass node
- wind direction compass node
- rotation needed gauge node
- text and audio output nodes

Then go to the dashboard and select the same group.  
The output should look like this:

![08_dashboard.png](./media/data_flow&calculations/08_dashboard.png)  


**&rarr; if done correctly**  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.) real time object detection repository transmits data  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.) tinkerforge weather station transmits data  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &rarr; servo motor gets new input angle over GPIO node
___

group contact information:  
noureldeen.nagm@rwth-aachen.de - Noureldeen Nagm  
dominik.leitner@rwth-aachen.de - Dominik Leitner


